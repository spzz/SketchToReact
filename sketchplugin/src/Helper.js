export function hasLayer(object) {
  return ((typeof object.layers === 'object') && (object.layers.length > 0))
}
export function exportLayer(sketch, layer, callback) {
  sketch.export(layer, {
    overwriting: true,
    formats: 'svg',
    output: "/tmp/test"
  }) || "undefined"
  callback("JAJA");
}
export function getSelectedLayer(object, callback) {
  var selectedAtrboard = undefined;
  if (typeof object.layers === 'object') {
    object
      .layers
      .map(function (entry) {
        if (entry.selected) {
          selectedAtrboard = entry
        }
      })
  }
  callback(selectedAtrboard)
}
export function readFileContent(filePath) {
  var fileManager = NSFileManager.defaultManager();
  if (fileManager.fileExistsAtPath(filePath)) {
    var data = NSString.stringWithContentsOfFile(filePath)
    // .stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)

    return data;
  }
  return "File Not Found"
}