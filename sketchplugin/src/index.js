const sketch = require('sketch')
const helper = require('./Helper')
const UI = require('sketch/ui')
const webui = require('./Webui')
import {isWebviewPresent, sendToWebview} from 'sketch-module-web-view/remote'
export default function getData(context) {
  const webUIId = "reactToskecthPopup"
  const document = sketch.fromNative(context.document)
  const page = document.selectedPage;

  if (typeof page === 'object') {
    helper
      .getSelectedLayer(page, function (selectedAtrBoard) {

        if (selectedAtrBoard != undefined) {
          var data = {
            JSON: selectedAtrBoard,
            SVG: ""
          }

          try {

            helper
              .exportLayer(sketch, selectedAtrBoard, function (exportContent) {
                setTimeout(function () {
                  try {
                    filepath = "/tmp/test/" + selectedAtrBoard.name + ".svg"
                  } catch (e) {
                    log(e)
                  }
                  webui.showWebUI(webUIId)
                  try {
                    setTimeout(function () {
                      try {
                        data["SVG"] = helper
                          .readFileContent(filepath)
                          .toString() + ""

                        sendToWebview(webUIId, `setPageJson(` + JSON.stringify(data) + `)`)
                      } catch (e) {
                        log("error Read File")
                        log(e)
                      }
                    }, 300)
                  } catch (e) {}
                }, 1000)

              })
          } catch (e) {
            log(e)
          }
        } else {
          UI.alert("Error", "Please Select The ArtBoar2d")
        }
      });

  } else {
    UI.alert("Error", "Error");
  }

}
// 1234 ;) i love you