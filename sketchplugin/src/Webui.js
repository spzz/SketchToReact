import WebUI from 'sketch-module-web-view'

export function showWebUI(id) {

    const webUI = new WebUI(context, require('../resources/webview.html'), {
        identifier: id, // to reuse the UI
        x: 0,
        y: 0,
        width: 700,
        height: 700,
        blurredBackground: true,
        onlyShowCloseButton: true,
        hideTitleBar: false,
        shouldKeepAround: true,
        resizable: true,
        frameLoadDelegate: { // https://developer.apple.com/reference/webkit/webframeloaddelegate?language=objc
            'webView:didFinishLoadForFrame:' (webView, webFrame) {
                context
                    .document
                    .showMessage('UI loaded!')

            }
        },
        handlers: {
            nativeLog(s) {
                context
                    .document
                    .showMessage(s)
            }
        }
    })
}