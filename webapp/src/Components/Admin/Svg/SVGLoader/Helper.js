// import {generateString} from "../../Helper/Service";

export function TagBuilder(data, index) {
    if ((data.name == undefined) && (typeof data !== 'object')) {
        return '';
    }
    var result = "<" + data.name;

    if (data.attrs) {
        data['info'] = {            
            objectName: "",
            objectType: "",
            collapsed: true,
            cssProperty: {}
        }
        for (var key in data.attrs) {
            result += " " + (key === 'xlinkHref'
                ? 'xlink:href'
                : key) + "\=\"" + data.attrs[key] + '"';
        }
    }
    result += ">";
    if (data.childs) {
        if ((data.name === "tspan") || (data.name === 'title') || (data.name === 'desc')) {
            result += " " + data.childs[0].text + " ";
        } else {
            data
                .childs
                .map(function (item, index) {
                    result += " " + TagBuilder(item, index) + " ";
                })
        }
    }
    result += "</" + data.name + ">";
    return result;
}

export function createChildMarkups(data) {
    var childs = "";
    if (typeof data !== 'object') {
        return '';
    }
    data
        .map(function (item, index) {
            childs += TagBuilder(item, index);
        });
    return childs;
}