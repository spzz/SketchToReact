import React, {Component} from 'react';
import Page, {Grid, GridColumn} from '@atlaskit/page';
import {createChildMarkups} from './SVGLoader/Helper'
import Layers from "./Layers";
import PageHeader from '../uiElement/PageHeader';
const svgson = require('svgson');

export default class Manager extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data
        }

        this.getChilds = this
            .getChilds
            .bind(this);
        this.handleClick = this
            .handleClick
            .bind(this);
    }
    getChilds(data) {
        return createChildMarkups(data)
    }

    handleClick(event) {
        console.log(event.target);
    }

    render() {
        const info = this.state.data.json;
        const svgInfo = this.state.data.parsedSvg;
        console.log(svgInfo)
        return (
            <Page>

                <Grid>
                    <GridColumn medium={4}>
                        <div>
                            <PageHeader>
                                Layers
                            </PageHeader>
                            <Layers data={svgInfo}/>
                        </div>
                    </GridColumn>
                    <GridColumn medium={7} className="center">
                        <div
                            style={{
                            textAlign: "center",
                            height: info.frame.height,
                            width: info.frame.width,
                            alignSelf: 'center'
                        }}>
                            <svg
                                width={svgInfo.attrs.width}
                                height={svgInfo.attrs.height}
                                viewBox={svgInfo.attrs.viewBox}
                                onClick={this.handleClick}
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                xlinkHref="http://www.w3.org/1999/xlink"
                                dangerouslySetInnerHTML={{
                                __html: this.getChilds(svgInfo.childs)
                            }}></svg>
                        </div>
                    </GridColumn>
                </Grid>

            </Page>
        )
    }
}