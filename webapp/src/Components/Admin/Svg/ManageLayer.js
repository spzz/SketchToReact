import React,{Component}from 'react';
import Symbol from './Symbol'
export default class ManageLayer extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const info=this.props.data;        
        switch(info.type){
            case 'SymbolInstance':
                return (<Symbol data={this.props.data}/>)
            break;
            default:
                return (
                    <div style={{height:info.frame.height,width:info.frame.width}}>
                        {info.name}
                    </div>
                )
            break;
        
        }
        
    }
}