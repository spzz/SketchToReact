import React, {Component} from 'react';
import TreeView from 'react-treeview';
export default class Layers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            layers: {},
            isLoad: false
        }
        this.setLayer = this
            .setLayer
            .bind(this)
        this.ToTreeView = this
            .ToTreeView
            .bind(this);
    }

    componentDidMount() {
        if (this.props.data) {
            this
                .props
                .data
                .childs
                .map(function (entry) {
                    if (entry.name == 'g') {
                        this.setLayer(entry)
                    }
                }.bind(this));
        }
    }

    setLayer(layer) {
        this
            .setState(function () {
                return {layers: layer, isLoad: true}
            })
    }
    ToTreeView(data) {
    
        var result = [];
        if ((typeof data.childs === 'object') && (data.childs.length > 0)) {

            data
                .childs
                .map(function (item, index) {

                    var child = [];
                    var id = index;
                    var label = "Label";
                    var collapsed = false;
                    if ((item.attrs) && (item.attrs.id)) {
                        id = item.attrs.id
                        label = item.attrs.id
                    }
                    if (item.info) {
                        id = item.info.objectId;
                        collapsed = item.info.collapsed;
                    }

                    result.push(
                        <TreeView nodeLabel={label + "||" + id} defaultCollapsed={collapsed} key={id}>
                            {this.ToTreeView(item)}
                        </TreeView>
                    )

                }.bind(this))
        }
        return result;
    }

    render() {
        if (!this.state.isLoad) {
            return "Loading Layers..."
        }
        return this.ToTreeView(this.state.layers)
    }
}