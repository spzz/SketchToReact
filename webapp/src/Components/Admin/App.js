import React, {Component} from 'react';
import {router} from '../../AppStructure/Routers/Router';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  render() {
    return (router())
  }
}

export default App;
