import React from 'react';
import {Route}from 'react-router-dom'
import ProjectsList from '../Admin/Projects/ProjectsList';
import Project from '../Admin/Projects/Project';
import Artboard from '../Admin/Projects/Artboard';
import { AdminContent } from './Styles/global.styled';
export const AdminRoute=()=>(
        <Route path="/admin"
            render={({ match: { url } }) => (            
                <AdminContent>                    
                    <Route exact path={`${url}`} component={ProjectsList} />                
                    <Route path={`${url}/project/:id`} component={Project} />                            
                    <Route path={`${url}/project-artboard/:projectId/:artboardindex`} component={Artboard} />                            
                </AdminContent>
            )}>
        </Route>
)