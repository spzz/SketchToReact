import styled from 'styled-components';
import Page from '@atlaskit/page'
import {Variables}from '../../../Helper/StyleGuide'
import { getHeight } from '../../../Helper/Helper';
let windowHeight=getHeight();
export const IntroPage=styled.div`    
    background-color:${Variables.Colors.Primary}
    height:${windowHeight+"px"}
    padding: ${(windowHeight/2)-250+"px"} 0;    
    text-align: center;
`
export const Section=styled.div`
    height:300px;
    padding:20px;
    background-color:white
    border-radius: 8px;
    text-align: center;
`
export const HeaderSection=styled.div`
    padding-bottom:30px;        
`