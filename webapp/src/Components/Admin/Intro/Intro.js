import React from 'react';
import Page, {
    Grid,
    GridColumn
} from '@atlaskit/page';
import{IntroPage,Section ,HeaderSection} from './Intro.styled'
import { getHeight } from '../../../Helper/Helper';
export default class Intro extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        
        return ( 
            <IntroPage>
                <Page>

                    <Grid>
                        <GridColumn medium={12}>
                            <HeaderSection>
                                <img height={100} src="https://raw.githubusercontent.com/nanohop/sketch-to-react-native/master/images/sketch_to_react_native.png"/>
                                <h1>
                                    Sketch To React 
                                </h1>
                                <p>
                                    Use The Skecth To React And Have a Dream Day With Family or (GF/BF) ;)
                                </p>
                            </HeaderSection>
                        </GridColumn>
                        <GridColumn medium={4}>
                            <Section>
                                <a href="/admin">
                                    <h5>
                                        Admin 
                                    </h5>
                                </a>
                            </Section>
                        </GridColumn>
                        <GridColumn medium={4}>
                            <Section>
                                <a href="/doc">
                                    <h5>
                                        Documention
                                    </h5>
                                </a>
                            </Section>
                        </GridColumn>
                        <GridColumn medium={4}>
                            <Section>
                                <a href="/doc">
                                    <h5>
                                        FeedBack
                                    </h5>
                                </a>
                            </Section>
                        </GridColumn>
                    </Grid>
                </Page>
            </IntroPage>
        )
    }
}