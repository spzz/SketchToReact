import React from 'react';
export const AppContext = React.createContext('App');

class AppProvider extends React.Component {
    constructor(props){
        super(props)
        this.setNewName=this.setNewName.bind(this);
        this.state = {
            app: {name:"SketchToReact",Author:"ParhamZare"},
            changeAppInfo:this.setNewName
        }
    }
    
    setNewName(app) {

        this.setState({app:app})
    }
    render() {
        return (
            <AppContext.Provider value={this.state} >
                {this.props.children}
            </AppContext.Provider>
        )
    }
}
export default AppProvider;
