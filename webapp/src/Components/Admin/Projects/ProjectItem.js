import React from 'react';
import Card from '../uiElement/Card';
import Href from '../uiElement/Href';
import Button from '../uiElement/Buttton';
import { CardFooter } from '../Styles/global.styled';
export default class ProjectItem extends React.Component{
    constructor(props){
        super(props)
    }
    render(){        
        return (
            <Card>
                <Href withprefix href="/project">
                    {this.props.data.name}
                </Href>
                <CardFooter>
                    <Href href={"/project/"+this.props.data._id} withprefix className="btn btn-warning">
                        Edit
                    </Href>                
                </CardFooter>
            </Card>
        )
    }
}
