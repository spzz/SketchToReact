import React from 'react';
import {ApiService} from './ProjectServices'
import ProjectItem from './ProjectItem';
import Page, {Grid, GridColumn} from '@atlaskit/page';
import PageHeader from '../uiElement/PageHeader';
import {AppContext}from '../Context/Context'
export default class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
        this.getData = this
            .getData
            .bind(this);

    }
    componentDidMount() {
        setTimeout(function () {
            this.getData();
        }.bind(this), 250)
    }

    getData() {
        ApiService
            .getAllProjects()
            .then(function (response) {
                this.setState({data: response.data})
            }.bind(this))
            .catch(function (err) {
                console.log(err.response)
            })

    }
    render() {
        return (
            <Page>

                <Grid>
                    <GridColumn medium={12}>
                        <PageHeader>List Projects</PageHeader>
                    </GridColumn>
                    <GridColumn medium={12}>
                        <Grid>
                            {this
                                .state
                                .data
                                .map(function (projectItem, index) {
                                    return (

                                        <GridColumn medium={4} key={projectItem._id}>
                                            <ProjectItem data={projectItem}/>
                                        </GridColumn>
                                    )
                                })}
                        </Grid>
                    </GridColumn>
                </Grid>
            </Page>
        )
    }
}