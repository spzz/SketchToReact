import React from 'react';
import {ApiService} from './ProjectServices';
import Page, {Grid, GridColumn} from '@atlaskit/page';
import PageHeader from '../uiElement/PageHeader';
import ArtboardItem from './ArtboardItem';
export default class ProjectForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoad: false,
            actionButtons: [
                {
                    id: "addArtBoard",
                    title: "New ArtBoard",
                    onClick: this.createNewArtBoard
                }
            ]
        };
        this.getData = this
            .getData
            .bind(this);
        this.createNewArtBoard = this
            .createNewArtBoard
            .bind(this);
    };
    createNewArtBoard() {
        alert("Lets Go To Create New ArtBoard")
    }
    componentDidMount() {
        setTimeout(function () {
            this.getData(this.props.match.params.id)
        }.bind(this), 250)
    }
    getData(id) {
        ApiService
            .getProjectInfo(id)
            .then(function (response) {
                let data = response.data[0]
                this.setState({data: data, isLoad: true});
            }.bind(this));
    }
    render() {
        if (!this.state.isLoad) {
            return null;
        }
        return (
            <Page>
                <Grid>
                    <GridColumn medium={12}>
                        <PageHeader buttons={this.state.actionButtons}>
                            {this.state.data.name}
                        </PageHeader>
                    </GridColumn>
                    <GridColumn medium={12}>
                        <Grid>
                            {this
                                .state
                                .data
                                .artboards
                                .map(function (artboard, index) {
                                    return (
                                        <GridColumn medium={4} key={"ArtBoard_" + index}>
                                            <ArtboardItem
                                                data={artboard}
                                                projectId={this.state.data._id}
                                                indexArtboard={index}/>
                                        </GridColumn>
                                    )
                                }.bind(this))}
                        </Grid>

                    </GridColumn>
                </Grid>
            </Page>
        )
    }
}