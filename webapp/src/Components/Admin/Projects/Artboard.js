import React from 'react';
import {ApiService} from './ProjectServices';
import Page, {Grid, GridColumn} from '@atlaskit/page';
import PageHeader from '../uiElement/PageHeader';
import Manager from '../Svg/Manager'

const svgson = require('svgson');

export default class ArtBoard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoad: false,
            actionButtons: [
                {
                    id: "See Code",
                    title: "See Code",
                    onClick: this.createNewArtBoard
                }
            ]
        }
        this.getData = this
            .getData
            .bind(this)
    }
    componentDidMount() {
        setTimeout(function () {
            this.getData(this.props.match.params.projectId, this.props.match.params.artboardindex);
        }.bind(this), 400)
    }
    getData(projectId, indexAttribute) {
        ApiService
            .getArtboard(projectId, indexAttribute)
            .then(function (response) {
                svgson(response.data.Svg, {}, function (parsedJson) {
                    this.setState({
                        data: Object.assign(response.data, {"parsedSvg": parsedJson}),
                        isLoad: true
                    });
                }.bind(this));
            }.bind(this))
    }
    render() {
        if (!this.state.isLoad) {
            return null;
        }
        return (
            <Page>
                <Grid>
                    <GridColumn medium={12}>
                        <PageHeader buttons={this.state.actionButtons}>
                            {this.state.data.name}
                        </PageHeader>
                    </GridColumn>
                    <GridColumn medium={12}>
                        <Manager data={this.state.data}/>
                    </GridColumn>
                </Grid>
            </Page>
        )
    }
}