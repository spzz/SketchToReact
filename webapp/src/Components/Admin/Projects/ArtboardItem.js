import React from 'react';
import {CardContent, CardFooter} from '../Styles/global.styled';
import Href from '../uiElement/Href'
export default class ArtboardItem extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <CardContent>
                {this.props.data.name}
                <CardFooter>
                    <Href
                        withprefix
                        href={"/project-artboard/" + this.props.projectId + "/" + this.props.indexArtboard}
                        className="btn btn-warning">
                        Edit
                    </Href>
                </CardFooter>
            </CardContent>
        )
    }
}
