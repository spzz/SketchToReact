import {
    getData,
    postData
} from '../../../Helper/Service'

function getAllProjects() {
    return getData('/projects')
}
function getProjectInfo(id){
    return getData('/project/'+id);
}
function getArtboard(projectId,artboardIndex){
    
    return getData('/project/artboard/'+projectId+"/"+artboardIndex)
}
export const ApiService={
    getAllProjects:getAllProjects,
    getProjectInfo:getProjectInfo,
    getArtboard:getArtboard
}