import styled from 'styled-components';
import Page, {
    Grid,
    GridColumn
} from '@atlaskit/page';
import {Variables}from '../../../Helper/StyleGuide'
import { getHeight } from '../../../Helper/Helper';
let windowHeight=getHeight();
export const AdminContent=styled.div`    
    background-color:${Variables.Colors.Primary}
    height:${windowHeight+"px"}
    padding: ${20+"px"} 0;    
`
export const HeaderSection=styled.div`
    border-bottom: 2px solid white;
    margin-bottom:10px
`
export const HeaderTitle=styled.h3`
    color:${Variables.Colors.White};
    
    
`
export const CardContent=styled.div`
    height:300px;
    padding:20px;
    background-color:white
    border-radius: 8px;        
`
export const CardFooter=styled.div`
    border-top:1px solid silver;
    position: absolute;
    bottom: 9px;
    width: 25%;
    padding: 10px;
    padding-left:0
`