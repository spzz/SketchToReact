import React from 'react';
export default class Href extends React.Component{
    constructor(props) {
        super(props)        
    }
    render(){
        
        return (
            <a {...this.props} href={(this.props.withprefix?"/admin":"")+this.props.href||"#"} >
                {this.props.children}
            </a>
        )
    }
}