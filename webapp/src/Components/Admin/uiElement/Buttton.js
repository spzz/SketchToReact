import React from 'react';
import Button from '@atlaskit/button'
export default class IButton extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <Button {...this.props} >

                {this.props.children}
            </Button>
        )
    }
}