import React from 'react';
import {CardContent}from '../Styles/global.styled'
export default class Card extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return (        
            <CardContent>                
                {this.props.children}
                
            </CardContent>
        )
    }
}