import React from 'react';
import {HeaderTitle, HeaderSection} from '../Styles/global.styled';
import Page, {Grid, GridColumn} from '@atlaskit/page';
import Button from './Buttton'
export default class PageHeader extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let buttons = [];
        if (this.props.buttons != undefined) {
            try {
                this
                    .props
                    .buttons
                    .map(function (btn, index) {
                        buttons.push(
                            <Button onClick={btn.onClick} key={"Btnindex"}>
                                {btn.title}
                            </Button>
                        )
                    })
            } catch (e) {}
        }
        return (
            <HeaderSection>
                <Grid medium={12}>
                    <GridColumn medium={6}>
                        <HeaderTitle>
                            {this.props.children}
                        </HeaderTitle>
                    </GridColumn>
                    <GridColumn medium={6}>
                        {buttons}
                    </GridColumn>
                </Grid>

            </HeaderSection>
        )
    }
}
