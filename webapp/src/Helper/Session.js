export function saveToLocalStorage(key, value) {
    localStorage.setItem(key, value)
}
export function getFromLocalStorage(key) {
    if (key !== undefined) {
        return localStorage.getItem(key) || undefined;
    }
}