import axios from 'axios';
const prefixUrl = "/api"
export function getData(url) {
    return axios.get(prefixUrl + url);
}
export function postData(url, data) {
    return axios.post(prefixUrl + url, data)
}