import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Intro from '../../Components/Admin/Intro'
import Header from '../../Components/Admin/Partials/Header'
import Footer from '../../Components/Admin/Partials/Footer'
import {AtlaskitThemeProvider, themed, colors} from '@atlaskit/theme';
import Button from '@atlaskit/button'
import {AdminRoute} from '../../Components/Admin/Route'
import Page, {Grid} from "@atlaskit/page"
import AppProvider from '../../Components/Admin/Context/Context'
export function router() {
    return (
        <AppProvider>

            <Router>
                <Page>
                    <Route exact path="/" key="intro" component={Intro}/>
                    <AdminRoute/>
                </Page>
            </Router>

        </AppProvider>

    );
}