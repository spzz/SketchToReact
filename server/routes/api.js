const apiController = require('../controllers/api');
const projectController = require('../controllers/ProjectController')
module.exports = function apiRoute(app, passportConfig, upload) {
    const prefixApi = "/api";
    /**
     * API examples routes.
     */
    app.get(`${prefixApi}`, apiController.getApi);
    app.get(`${prefixApi}/projects`, projectController.getProjects);
    app.get(`${prefixApi}/project/:id`, projectController.getProjectInfo);
    app.get(`${prefixApi}/project/artboard/:projectId/:indexArtboard`, projectController.getProjectArtboard);
    app.get(`${prefixApi}/import`, projectController.importJsonFile);
    app.get(`${prefixApi}/transform`, projectController.transformSvg);

    app.get(`${prefixApi}/lastfm`, apiController.getLastfm);
    app.get(`${prefixApi}/nyt`, apiController.getNewYorkTimes);
    app.get(`${prefixApi}/aviary`, apiController.getAviary);
    app.get(`${prefixApi}/steam`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getSteam);
    app.get(`${prefixApi}/stripe`, apiController.getStripe);
    app.post(`${prefixApi}/stripe`, apiController.postStripe);
    app.get(`${prefixApi}/scraping`, apiController.getScraping);
    app.get(`${prefixApi}/twilio`, apiController.getTwilio);
    app.post(`${prefixApi}/twilio`, apiController.postTwilio);
    app.get(`${prefixApi}/clockwork`, apiController.getClockwork);
    app.post(`${prefixApi}/clockwork`, apiController.postClockwork);
    app.get(`${prefixApi}/foursquare`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getFoursquare);
    app.get(`${prefixApi}/tumblr`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getTumblr);
    app.get(`${prefixApi}/facebook`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getFacebook);
    app.get(`${prefixApi}/github`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getGithub);
    app.get(`${prefixApi}/twitter`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getTwitter);
    app.post(`${prefixApi}/twitter`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.postTwitter);
    app.get(`${prefixApi}/linkedin`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getLinkedin);
    app.get(`${prefixApi}/instagram`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getInstagram);
    app.get(`${prefixApi}/paypal`, apiController.getPayPal);
    app.get(`${prefixApi}/paypal/success`, apiController.getPayPalSuccess);
    app.get(`${prefixApi}/paypal/cancel`, apiController.getPayPalCancel);
    app.get(`${prefixApi}/lob`, apiController.getLob);
    app.get(`${prefixApi}/upload`, apiController.getFileUpload);
    app.post(`${prefixApi}/upload`, upload.single(`myFile`), apiController.postFileUpload);
    app.get(`${prefixApi}/pinterest`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getPinterest);
    app.post(`${prefixApi}/pinterest`, passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.postPinterest);
    app.get(`${prefixApi}/google-maps`, apiController.getGoogleMaps);
}