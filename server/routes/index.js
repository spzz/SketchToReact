const webRouter = require('./webroutes');
const apiRouter = require('./api');
const socialLoginRouter = require('./socialLoginRoutes')
module.exports = {
    webRouter: webRouter,
    apiRouter: apiRouter,
    socialLoginRouter: socialLoginRouter
}
