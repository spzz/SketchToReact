const passport = require('passport');
const Projects = require('../models/Projects/Projects');
const svgr=require('@svgr/core').default;
exports.getProjects = (req, res) => {

    Projects
        .find({}, [
            "name", "id", "description"
        ], function (err, projects) {
            if (err) {
                res.send([]);
            }
            res.json(projects);
        })
}
exports.getProjectInfo = (req, res) => {
    const {id} = req.params;

    Projects.find({
        _id: id
    }, function (err, project) {
        if (err) {
            res.send([]);
        }
        res.json(project);
    })
}
exports.getProjectArtboard = (req, res) => {
    const {projectId, indexArtboard} = req.params;
    Projects.find({
        _id: projectId
    }, function (err, project) {
        if (err) {
            res.send([]);
        }
        if (project[0].artboards != undefined) {
            res.json(project[0].artboards[indexArtboard]);
            return true;
        }
        res.send({})
        return false;
    })
}
exports.importJsonFile = (req, res) => {
    try {
        let IdProject = "5b41177740ee9802e78cdd26"
        Projects.find({
            _id: IdProject
        }, ['artboards'], function (err, project) {
            let artboards = [];
            if (project[0].artboards != undefined) {
                artboards = project[0].artboards;
            }
            const {JSON, SVG} = require('../models/a.json');
            artboards.push({name: JSON.name, json: JSON, Svg: SVG})

            Projects.updateOne({
                '_id': IdProject
            }, {
                "artboards": artboards
            }, function (err, updateResult) {
                res.send(updateResult);
            })

        });
        return true;
    } catch (e) {
        return res.send("Fail")
    }
}
exports.transformSvg=(req,res)=>{
    try {
        let IdProject = "5b31103068b63c427800191c"
        Projects.find({
            _id: IdProject
        }, ['artboards'], function (err, project) {
            let artboards = project[0].artboards[0].Svg; 
            svgr(artboards, { icon: false ,native:true,dimensions:false,  "svgoConfig": {
    "plugins": [{ "cleanupIDs": false }, { "removeUselessDefs": false }]
  }}, { componentName: project[0].artboards[0].name }).then(function(jsCode){
                return res.send(jsCode)
            });     
        });
        return true;
    } catch (e) {
        return res.send("Fail")
    }

}